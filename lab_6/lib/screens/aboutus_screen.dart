import 'package:flutter/material.dart';

import '../widgets/main_drawer.dart';


class AboutUsScreen extends StatefulWidget {
  static const routeName = '/aboutus';

  AboutUsScreen();

  @override
  _AboutUsScreenState createState() => _AboutUsScreenState();
}

class _AboutUsScreenState extends State<AboutUsScreen> {


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Tentang Kami'),
      ),
      drawer: MainDrawer(),
      body: Center(
        child: Column(
          children: <Widget>[ Text(
            'About Us',
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 40),
          ),
          Text(
            'Ini adalah apliaksi yang memuat berbagai resep makanna. So Yummy....',
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 20),
          ),
        ]
        ),
        
        
        
      ),
    );
  }
}