import 'package:flutter/material.dart';
import '../screens/tabs_screen.dart';

class ContactUsScreen extends StatefulWidget {
  static const routeName = '/contactus';

  ContactUsScreen();

  @override
  _ContactUsScreenState createState() => _ContactUsScreenState();
}

class _ContactUsScreenState extends State<ContactUsScreen> {
  

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text('Hubungi Kami'),
      ),
      body: Center(
        child: Text(
            '0856-XXX-XXX',
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 35),
        ),
      ),
    );
  }
}
